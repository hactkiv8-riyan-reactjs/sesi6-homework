import React from 'react';
import './styles/bootstrap/bootstrap.css';
import './styles/gfonts/opensans.css';
import './styles/gfonts/saira.css';
import './styles/font-awesome/css/font-awesome.min.css';
import './styles/devicons/css/devicons.min.css';
import './styles/simple-line-icons/css/simple-line-icons.css';
import './styles/style.global.css';
import SideNav from './components/SideNav';
import About from './components/About';
import { BrowserRouter } from 'react-router-dom';
import Experience from './components/Experience';
import Portfolio from './components/Portofolio';
import Skills from './components/Skills';
import Awards from './components/Awards';
import Contact from './components/Contact';

class App extends React.Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <SideNav />
          <About />
          <Experience />
          <Portfolio />
          <Skills />
          <Awards />
          <Contact />
        </div>
      </BrowserRouter>
    )
  }

}

export default App;
