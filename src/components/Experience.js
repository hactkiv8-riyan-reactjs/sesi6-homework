import React, { Component } from 'react';

export default class Experience extends Component {
    render() {
        return (
            <section className="resume-section p-3 p-lg-5 d-flex d-column" id="experience">
                <div className="row my-auto">
                    <div className="col-12">
                        <h2 className="  text-center">Experience</h2>
                        <div className="mb-5 heading-border"></div>
                    </div>
                    <div className="resume-item col-md-6 col-sm-12 " >
                        <div className="card mx-0 p-4 mb-5" style={{ borderColor: '#17a2b8', boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.21)' }}>
                            <div className=" resume-content mr-auto">
                                <h4 className="mb-3"><i className="fa fa-globe mr-3 text-info"></i> Web Developer</h4>
                                <p>programmer who specializes in the development of applications relating to the World Wide Web or distributed network applications, which typically run protocols like HTTP from a Web server to a client browser using associated programming languages like HTML/CSS, C#, Ruby and PHP to name a few.</p>
                            </div>
                            <div className="resume-date text-md-right">
                                <span className="text-primary">April 2016 - Present</span>
                            </div>
                        </div>
                    </div>
                    <div className="resume-item col-md-6 col-sm-12">
                        <div className="card mx-0 p-4 mb-5" style={{ borderColor: '#ffc107', boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.21)' }}>
                            <div className="resume-content mr-auto">
                                <h4 className="mb-3"><i className="fa fa-laptop mr-3 text-warning"></i>  Mobile Developer</h4>
                                <p>A mobile developer creates software for mobile devices and technology. Whether for an Android, Apple or Windows platform, a mobile developer must learn the software development environment and programming languages for their chosen platform.</p>
                            </div>
                            <div className="resume-date text-md-right">
                                <span className="text-primary">April 2016 - Present</span>
                            </div>
                        </div>
                    </div>
                    <div className="resume-item col-md-6 col-sm-12">
                        <div className="card mx-0 p-4 mb-5" style={{ borderColor: '#ffc107', boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.21)' }}>
                            <div className="resume-content mr-auto">
                                <h4 className="mb-3"><i className="fa fa-laptop mr-3 text-warning"></i>  Game Developer</h4>
                                <p>A mobile developer creates software for mobile devices and technology. Whether for an Android, Apple or Windows platform, a mobile developer must learn the software development environment and programming languages for their chosen platform.</p>
                            </div>
                            <div className="resume-date text-md-right">
                                <span className="text-primary">April 2016 - Present</span>
                            </div>
                        </div>
                    </div>
                    <div className="resume-item col-md-6 col-sm-12">
                        <div className="card mx-0 p-4 mb-5" style={{ borderColor: '#ffc107', boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.21)' }}>
                            <div className="resume-content mr-auto">
                                <h4 className="mb-3"><i className="fa fa-laptop mr-3 text-warning"></i>  Adobe Flash Developer</h4>
                                <p>A mobile developer creates software for mobile devices and technology. Whether for an Android, Apple or Windows platform, a mobile developer must learn the software development environment and programming languages for their chosen platform.</p>
                            </div>
                            <div className="resume-date text-md-right">
                                <span className="text-primary">April 2016 - Present</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}