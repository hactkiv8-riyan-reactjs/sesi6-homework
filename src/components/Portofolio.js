import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Portfolio extends Component {
    render() {
        return (
            <section className="resume-section p-3 p-lg-5 d-flex flex-column" id="portfolio">
                <div className="row my-auto">
                    <div className="col-12">
                        <h2 className="  text-center">Portfolio</h2>
                        <div className="mb-5 heading-border"></div>
                    </div>
                    <div className="col-md-12">
                        <div className="port-head-cont">
                            <button className="btn btn-general btn-green filter-b" data-filter="all">All</button>
                            <button className="btn btn-general btn-green filter-b" data-filter="consulting">Web Design</button>
                            <button className="btn btn-general btn-green filter-b" data-filter="finance">Mobile Apps</button>
                            <button className="btn btn-general btn-green filter-b" data-filter="marketing">Graphics Design</button>
                        </div>
                    </div>
                </div>
                <div className="row my-auto">
                    <div className="col-sm-4 portfolio-item filter finance">
                        <Link className="portfolio-link" to="#portfolioModal4" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require('../assets/img/portfolio/p-4.jpg')} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter marketing">
                        <Link className="portfolio-link" to="#portfolioModal5" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-5.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter consulting">
                        <Link className="portfolio-link" to="#portfolioModal6" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-6.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter consulting">
                        <Link className="portfolio-link" to="#portfolioModal7" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-7.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter consulting">
                        <Link className="portfolio-link" to="#portfolioModal8" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-8.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter finance">
                        <Link className="portfolio-link" to="#portfolioModal9" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-9.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter marketing">
                        <Link className="portfolio-link" to="#portfolioModal1" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-1.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter marketing">
                        <Link className="portfolio-link" to="#portfolioModal2" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-2.jpg")} alt="" />
                        </Link>
                    </div>
                    <div className="col-sm-4 portfolio-item filter finance">
                        <Link className="portfolio-link" to="#portfolioModal3" data-toggle="modal">
                            <div className="caption-port">
                                <div className="caption-port-content">
                                    <i className="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <img className="img-fluid" src={require("../assets/img/portfolio/p-3.jpg")} alt="" />
                        </Link>
                    </div>
                </div>
            </section>
        )
    }
}