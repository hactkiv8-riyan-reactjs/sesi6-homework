import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class About extends Component {
    render() {
        return (

            <div className="container-fluid p-0">
                <section className="resume-section p-3 p-lg-5 d-flex d-column" id="about">
                    <div className="my-auto">
                        <img src={require('../assets/img/logo-s.png')} className="img-fluid mb-3" alt="" />
                        <h1 className="mb-0">Riyan
                        <span className="text-primary"> Saputra Irawan</span>
                        </h1>
                        <div className="subheading mb-5" style={{ color: '#fff' }}>THE NEXT BIG IDEA IS WAITING FOR ITS NEXT BIG CHANGER WITH
                         <a >THEMSBIT</a>
                        </div>
                        <p className="mb-5" style={{ maxWidth: 500, color: '#fff' }} >I am experienced in software development. especially development for web and mobile software</p>
                        <ul className="list-inline list-social-icons mb-0">
                            <li className="list-inline-item">
                                <Link>
                                    <span className="fa-stack fa-lg">
                                        <i className="fa fa-circle fa-stack-2x"></i>
                                        <i className="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                    </span>
                                </Link>
                            </li>
                            <li className="list-inline-item">
                                <Link >
                                    <span className="fa-stack fa-lg">
                                        <i className="fa fa-circle fa-stack-2x"></i>
                                        <i className="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                    </span>
                                </Link>
                            </li>
                            <li className="list-inline-item">
                                <Link>
                                    <span className="fa-stack fa-lg">
                                        <i className="fa fa-circle fa-stack-2x"></i>
                                        <i className="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                    </span>
                                </Link>
                            </li>
                            <li className="list-inline-item">
                                <Link >
                                    <span className="fa-stack fa-lg">
                                        <i className="fa fa-circle fa-stack-2x"></i>
                                        <i className="fa fa-github fa-stack-1x fa-inverse"></i>
                                    </span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        )
    }
}